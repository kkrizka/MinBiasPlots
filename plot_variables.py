"""
Plot and compare different variables between input samples.
"""
# %%
from mbplot import config

from kkconfig import runconfig
import kkplot

import matplotlib.pyplot as plt
import uproot
import sys

# %% ASDF
fh=uproot.open(f'{config.datadir}/data22_13p6TeV.00427929.physics_MinBias.recon.IDPVM.r14288.root')
h=fh['SquirrelPlots/Vertices/AllPrimaryVertices/vx_x']

# %%
histcommon={'histtype':'step'}

# %%
fig,ax = plt.subplots()

kkplot.hist(h.axis().edges(),h.values(), **histcommon)

fig.show()
# %% Prepare configuration
if 'ipykernel' in sys.modules: # running in a notebook
    # %load_ext autoreload
    # %autoreload 2
    runcfgpaths=['runconfigs/lowmu_matvar_doMinBias_True_doVeryLowPt_True.yaml', 'runconfigs/vars_trackprops.yaml']
else:
    if len(sys.argv)<2:
        print('usage: {} runconfig.yaml [runconfig.yaml]'.format(sys.argv[0]))
        sys.exit(1)
    runcfgpaths=sys.argv[1:]

runcfg = tools.load_runconfigs(runcfgpaths)

# %% Open input files
# They have to remain open by the drawing time so the TH1 objects still exist
# in memory.
# Also prevents opening all files multiple times.
for i in runcfg['inputs']:
    rdir=i.get('rdir','SquirrelPlots')

    path=config.datapath+'/'+i['path']
    fh=ROOT.TFile.Open(path)
    i['fh']=fh

# %%
for v in runcfg['variables']:
    print('Plotting variable {}'.format(v['name']))
    c=plot.create_canvas_ratio()

    hs=ROOT.THStack()
    hsrat=ROOT.THStack()
    href=None # histgram to serve as reference for ratio plot

    l = ROOT.TLegend(0.6,0.6+len(runcfg['inputs'])*0.05,0.9,0.6)
    l.SetBorderSize(0)

    for i in runcfg['inputs']:
        hvar=i['fh'].Get(rdir+'/Tracks/Selected/'+v['name'])
        hvar.Scale(1./hvar.Integral())

        # First entry serves as the reference
        if href is None:
            href=hvar

        # Apply styles
        style.style(hvar, **i)

        # Add to legend
        l.AddEntry(hvar,i['title'],'l')

        # Add to stack
        hs.Add(hvar)

        # Add to ratio plot
        hrat=hvar.Clone(hvar.GetName()+'_ratio')
        hrat.Divide(href)
        hsrat.Add(hrat)

    # Main plot
    c.cd(1)
    hs.Draw('NOSTACK HIST')
    l.Draw()

    hs.GetYaxis().SetTitle('normalized')

    # Ratio plot
    c.cd(2)
    hsrat.Draw('NOSTACK HIST')

    hsrat.GetYaxis().SetTitle('ratio')
    hsrat.SetMinimum(0.95)
    hsrat.SetMaximum(1.05)
    hsrat.GetXaxis().SetTitle(href.GetXaxis().GetTitle())

    # Final styling
    plot.apply_ratio_axis_style(c)

    c.Draw()
    fname=v['name'].replace('/','_') # Flat directory structure
    c.SaveAs(fname+'.'+config.format)

# %%
