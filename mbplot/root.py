"""
Functions to draw ROOT objects (from uproot) using matplotlib.
"""

import matplotlib.pyplot as plt

def hist(h, ax=plt, **kwargs):
    """
    Draw a histogram object (ie: TH1).

    A histogram object is any object with the following properties:
     - axis(0).edges() : returns the bin edges
     - values(): returns the bin contents
     - errors(): returns the bin uncertainties

    Parameters
    ----------
    h : histogram object
        The histogram object to draw.
    ax : AxesSubplot
        Canvas to draw on.
    **kwargs : dict
        Passed to the drawing function.
    """
    edges=h.axis(0).edges()
    x=(edges[:-1]+edges[1:])/2
    xerr=(edges[1:]-edges[:-1])/2
    y=h.values()
    yerr=h.errors()

    ax.errorbar(x,y,xerr=xerr,yerr=yerr,fmt=',',**kwargs)