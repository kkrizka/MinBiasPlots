"""
Plot and compare different variables between input samples.
"""

# %%
from mbplot import config
from mbplot import root

from kkconfig import runconfig
import kkplot

from copy import deepcopy

import numpy as np
import matplotlib.pyplot as plt
import uproot
import sys

# %% Prepare configuration
if 'ipykernel' in sys.modules: # running in a notebook
    # %load_ext autoreload
    # %autoreload 2
    runcfgpaths=['runconfigs/datamc.yaml', 'runconfigs/variables.yaml']
else:
    if len(sys.argv)<2:
        print('usage: {} runconfig.yaml [runconfig.yaml]'.format(sys.argv[0]))
        sys.exit(1)
    runcfgpaths=sys.argv[1:]

runcfg = runconfig.load(runcfgpaths)

# %% Open input files
# Prevents opening all files multiple times.
for i in runcfg['inputs']:
    path=config.datapath+'/'+i['path']
    fh=uproot.open(path)
    i['fh']=fh

# %%
for v in runcfg['variables']:
    print(f'Plotting variable {v["name"]}')

    fig, ax = kkplot.subplot_ratio()

    # Plot the profiles
    ref_prof=None # for the ratio
    prof=None
    for i in runcfg['inputs']:
        rdir=i.get('rdir','SquirrelPlots')
        prof=i['fh'][rdir+'/Tracks/Selected/'+v['name']]

        color=i.get('color',None)

        root.hist(prof, color=color, label=i.get('title',None), ax=ax[0])

        # Plot the ratio
        if ref_prof is None:
            ref_prof=prof

        ratprof=deepcopy(prof)

        r=ratprof.values()
        r/=ref_prof.values()

        rerr=ratprof.errors()
        rerr[:]=np.sqrt(rerr**2+ref_prof.errors()**2)

        root.hist(ratprof, color=color, ax=ax[1])

    # Style the axes
    ax[1].set_xlabel(v.get('xlabel',None))
    ax[1].set_ylabel('ratio')
    ax[0].set_ylabel(v.get('ylabel',None))

    ax[0].set_xlim(v.get('xlim',None))
    if 'xticks' in v:
        kkplot.xticks(*v.get('xticks',(None,None)),ax=ax[0])
        kkplot.xticks(*v.get('xticks',(None,None)),ax=ax[1])

    ax[1].set_ylim(*v.get('rlim',(None,None)))
    if 'rticks' in v:
        kkplot.yticks(*v.get('rticks',(None,None)),ax=ax[1])

    ax[0].legend()

    fig.show()

    fname=v['name'].replace('/','_') # Flat directory structure
    config.format='png'
    fig.savefig(fname+'.'+config.format)

# %%
